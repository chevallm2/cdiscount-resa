import React, {Component} from 'react';
import './App.css';
import './materialize/css/materialize.min.css';
const su = require('stringutils');


import Navbar from './Navbar';
import Footer from './Footer';
import Card from './Card';
import ListeDesSalles from './ListeDesSalles';

class MainScreen extends Component {

  constructor(props) {
    super(props);
    this.textes = {
      etatsDesSalles: {
        titre: "états des salles",
      }
    }
  }

  render() {
    return (
      <div>
        <Navbar/>
        <main className="container">
          <Card titre={su.capitalize(this.textes.etatsDesSalles.titre)} contenu={<ListeDesSalles/>}/>
        </main>
        <Footer/>
      </div>
    );
  }
}

export default MainScreen;
