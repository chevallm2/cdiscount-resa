import React, {
    Component
} from 'react';

import './materialize/css/materialize.min.css';

import logoCDiscount from './images/cdiscount-logo.png';

class Footer extends Component {

  constructor(props) {
    super(props);
    this.textes = {
      informations: "Cette application sert à la gestion de la réservation des salles. Vous pouvez ici gérer la réservation, l'annulation et la modification de vos salles.",
      copyright: "MCP & GF © 2017 pour Cdiscount"
      } 
    }


  render() {
    return (
      <footer className="page-footer blue">
        <div className="container">
          <div className="row">
            <div className="col l6 s12">
              <h5 className="white-text"><img src={logoCDiscount} alt="Logo de Cdiscount"/></h5>
              <p className="grey-text text-lighten-4">{this.textes.informations}</p>
            </div>
            <div className="col l4 offset-l2 s12">
              <h5 className="white-text">Liens</h5>
              <ul>
                <li><a className="grey-text text-lighten-3" href="http://www.cdiscount.com/"> Site officiel</a></li>
              </ul>
            </div>
          </div>
        </div>
        <div className="footer-copyright">
          <div className="container">
          {this.textes.copyright}
          </div>
        </div>
      </footer>
    );
  }
}

export default Footer;
