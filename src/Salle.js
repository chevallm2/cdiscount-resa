import React, {Component} from 'react';
import './materialize/css/materialize.min.css';
import { Link } from 'react-router-dom';

class ListeDesSalles extends Component {

    constructor(props) {
      super(props);
      this.salle = {
        nom: props.data.nom,
        etat: props.data.etat,
        emplacement: props.data.emplacement,
        disponibilite: props.data.disponibilite,
        reservations: props.data.reservations,
      };
      (this.salle.etat) ? this.etatLabel = "Libre" : this.etatLabel = "Occupé";
      (this.salle.etat) ? this.couleur = "green" : this.couleur = "red";

      this.url = "/room/" + this.salle.nom;
      this.reservations = [];
      if(this.salle.reservations.length > 0) {
        this.salle.reservations.forEach( (reservation) => {
          this.reservations.push(<li className="collection-item"><p>{reservation.date} par <a className="blue-text text-darken-2" href="#">{reservation.organisateur}</a><a className="secondary-content red-text" href="#" onClick="">X</a></p></li>);
          console.dir(reservation);
        });

      } else {
        this.reservations = (
          <li className="collection-item">Aucune réservations aujourd'hui</li>
        )
      }

    }

    render() {
        return (
          <div className={"card " + this.couleur + " lighten-1"}>
            <div className="card-content">
              <span className="card-title">
                {this.salle.nom} ({this.etatLabel}) (<Link to={this.url} >Voir plus</Link>)
              </span>
              <div className="row">
                <div className="col s12 m3">
                  <p>{this.salle.emplacement}</p>
                  <p>{this.salle.disponibilite}</p>
                </div>
                <div className="col s12 m9">
                  <h5>Réservations</h5>
                  <ul className="collection">
                    <div>{this.reservations}</div>
                  </ul>
                </div>
                <a className="btn-floating btn-large blue right">+</a>
              </div>
            </div>
          </div>
        );
    }
}

export default ListeDesSalles;
