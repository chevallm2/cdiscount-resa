import React, {Component} from 'react';

import './materialize/css/materialize.min.css';

import Salle from './Salle';

class ListeDesSalles extends Component {

    constructor(props) {
      super(props);
      const donneesDesSalles =
      [
        {
          nom: "Saphir",
          etat: true,
          emplacement: "Rez-de-chaussé",
          disponibilite: "Occupé dans 5 minutes",
          reservations: [
            {
              date: "Lundi 1 Juillet de 9:30 à 10:00",
              organisateur: "Maxime Chevallier-Pichon"
            },
            {
              date: "Lundi 1 Juillet de 15:00 à 17:55",
              organisateur: "Gabriel Fruhauf"
            }
          ],
        },
        {
          nom: "Rubis",
          etat: false,
          emplacement: "1er étage",
          disponibilite: "Libre dans 2 heures et 15 minutes",
          reservations: [
            {
              date: "Lundi 1 Juillet de 14:00 à 17:00",
              organisateur: "Gabriel Fruhauf"
            }
          ],
        },
        {
          nom: "Émeraude",
          etat: true,
          emplacement: "1er étage",
          disponibilite: "Libre toute la journée",
          reservations: [],
        },
        {
          nom: "Diamant",
          etat: false,
          emplacement: "Batiment B",
          disponibilite: "En travaux",
          reservations: [],
        }
      ];
      this.nb = [];
      donneesDesSalles.forEach( (salle) => {
        this.nb.push(<Salle key={salle.nom} data={salle}/>);
      });
    }

    render() {
        return (
          <div>
            <ul>
              <li>{this.nb}</li>
            </ul>
          </div>
        );
    }
}

export default ListeDesSalles;
