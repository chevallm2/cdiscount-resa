import React, {Component} from 'react';
import './App.css';
import './materialize/css/materialize.min.css';


import './RoomScreen.css';

import Navbar from './Navbar';
import Footer from './Footer';

class RoomScreen extends Component {

  constructor(props) {
    super(props);
    this.salles = [
      {
        nom: "Saphir",
        etat: true,
        emplacement: "Rez-de-chaussé",
        disponibilite: "Occupé dans 5 minutes",
        reservations: [
          {
            date: "Lundi 1 Juillet de 9:30 à 10:00",
            organisateur: "Maxime Chevallier-Pichon"
          },
          {
            date: "Lundi 1 Juillet de 15:00 à 17:55",
            organisateur: "Gabriel Fruhauf"
          }
        ],
      },
      {
        nom: "Rubis",
        etat: false,
        emplacement: "1er étage",
        disponibilite: "Libre dans 2 heures et 15 minutes",
        reservations: [
          {
            date: "Lundi 1 Juillet de 14:00 à 17:00",
            organisateur: "Gabriel Fruhauf"
          }
        ],
      },
      {
        nom: "Émeraude",
        etat: true,
        emplacement: "1er étage",
        disponibilite: "Libre toute la journée",
        reservations: [],
      },
      {
        nom: "Diamant",
        etat: false,
        emplacement: "Batiment B",
        disponibilite: "En travaux",
        reservations: [],
      }
    ];

    this.salles.forEach(salle => {
      if(salle.nom === this.props.match.params.nom) {
        this.salle = salle;
      }
    });
    (this.salle.disponibilite) ? this.etat = "Libre" : this.etat = "Occupé";
  }

  render() {
    return (
      <div>
        <Navbar/>
        <main className="container">
          <div className="card">
            <div className="card-content">
              <div className="card-title">
                {this.salle.nom} ({this.etat})
              </div>
              <div className="row">
              <table className="responsive-table highlight centered bordered">
                  <thead>
                    <tr>
                      <th className="center">8:00 à 9:00</th>
                      <th className="center">9:00 à 10:00</th>
                      <th className="center">10:00 à 11:00</th>
                      <th className="center">11:00 à 12:00</th>
                      <th className="center">12:00 à 13:00</th>
                      <th className="center">13:00 à 14:00</th>
                      <th className="center">14:00 à 15:00</th>
                      <th className="center">15:00 à 16:00</th>
                      <th className="center">16:00 à 17:00</th>
                      <th className="center">17:00 à 18:00</th>
                      <th className="center">18:00 à 19:00</th>
                    </tr>
                  </thead>
                  <tbody>
                    <tr>
                      <td className="hoverable red"></td>
                      <td className="hoverable red"></td>
                      <td className="hoverable green"></td>
                      <td className="hoverable green"></td>
                      <td className="hoverable green"></td>
                      <td className="hoverable green"></td>
                      <td className="hoverable red"></td>
                      <td className="hoverable green"></td>
                      <td className="hoverable green"></td>
                      <td className="hoverable green"></td>
                      <td className="hoverable green"></td>
                    </tr>
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </main>
        <Footer/>
      </div>
    );
  }
}

export default RoomScreen;
