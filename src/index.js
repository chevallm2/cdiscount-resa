import React from 'react';
import ReactDOM from 'react-dom';
import { Switch, Route, BrowserRouter as Router } from 'react-router-dom';
import './index.css';
import MainScreen from './MainScreen';
import RoomScreen from './RoomScreen';

const RoutingTest = () => (
    <Router>
        <Switch>
            <Route path='/room/:nom' component={RoomScreen}/>
            <Route path='/' component={MainScreen}/>
        </Switch>
    </Router>
);

ReactDOM.render(
  <RoutingTest />,
  document.getElementById('root')
);
