import React, {Component} from 'react';

import './materialize/css/materialize.min.css';


class Card extends Component {

  constructor(props){
      super(props);
      this.classes = "card col 12 " + this.props.couleur;
  }

  render() {
    return (
      <div className={this.classes} >
        <div className="card-content">
          <div className="card-title">
            {this.props.titre}
          </div>
          <div>
           {this.props.contenu}
          </div>
        </div>
      </div>
    );
  }
}

export default Card;
