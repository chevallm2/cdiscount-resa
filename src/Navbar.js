import React, {
    Component
} from 'react';

import './materialize/css/materialize.min.css';
import './Navbar.css';

import logoCDiscount from './images/cdiscount-logo.png';

class Navbar extends Component {
  render() {
    return (
        <nav className="blue">
          <div className="nav-wrapper">
            <a href="#" className="brand-logo center"><img src={logoCDiscount} alt="Logo Cdiscount" title="Retour à l'accueil"/></a>
            <a href="#" data-activates="mobile-demo" className="button-collapse"><i className="material-icons">menu</i></a>
              <ul className="right hide-on-med-and-down">
                <li><a href="#">Connexion</a></li>
              </ul>
              <ul className="side-nav" id="mobile-demo">
                <li><a href="/">Connexion</a></li>
              </ul>
          </div>
        </nav>
    );
  }
}

export default Navbar;
