import React, {
    Component
} from 'react';
import './App.css';
import './materialize/css/materialize.min.css';


import MainScreen from './MainScreen';

class App extends Component {
    render() {
        return (
          <div className="App grey lighten-2">
            <MainScreen/>
          </div>
        );
    }
}

export default App;
